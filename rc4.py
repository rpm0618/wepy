#!/usr/bin/python

import utilities

def RC4(seed, length):
	"""A Generator that returns an RC4 keystream"""
	S = range(256)
	seedLen = len(seed)

	j = 0
	for i in xrange(256):
		j = (j + S[i] + seed[i % seedLen]) % 256
		S[i], S[j] = S[j], S[i]

	i, j = 0, 0
	for x in xrange(length):
		i = (i + 1) % 256
		j = (j + S[i]) % 256
		S[i], S[j] = S[j], S[i]
		yield S[(S[i] + S[j]) % 256]

def wepRC4(iv, key, length=1):
	"""A Convenience method, just appends the IV to the key"""
	seed = iv[:]
	seed.extend(key)
	return list(RC4(seed, length))