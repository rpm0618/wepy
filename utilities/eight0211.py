#!/usr/bin/python
"""Utility methods having to deal with 802.11 wireless packets"""

def getBSSID(pkt):
	"""Return the BSSID of the access point that this packet belongs to"""

	toDS = (pkt.FCfield & 1) != 0
	fromDS = (pkt.FCfield & 2) != 0

	if fromDS and not toDS:
		return pkt.addr2
	elif not fromDS and not toDS:
		return pkt.addr3
	elif not fromDS and toDS:
		return pkt.addr1

def getDestination(pkt):
	"""Return the Destination MAC of this packet"""

	toDS = (pkt.FCfield & 1) != 0
	fromDS = (pkt.FCfield & 2) != 0

	if fromDS and not toDS:
		return pkt.addr1
	elif not fromDS and not toDS:
		return pkt.addr1
	elif not fromDS and toDS:
		return pkt.addr3

def getSource(pkt):
	"""Return the Destination MAC of this packet"""
	
	toDS = (pkt.FCfield & 1) != 0
	fromDS = (pkt.FCfield & 2) != 0

	if fromDS and not toDS:
		return pkt.addr3
	elif not fromDS and not toDS:
		return pkt.addr2
	elif not fromDS and toDS:
		return pkt.addr2