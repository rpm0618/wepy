#!/usr/bin/python
"""Utility methods dealing with permutations"""

def inversePerm(perm):
	"""Invert a permutation	"""
	inverse = [0] * len(perm)
	for i, p in enumerate(perm):
		inverse[p] = i
	return inverse