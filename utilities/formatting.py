#!/usr/bin/python
"""Utility methods to format strings from other values"""

def byteToHex(byteString, sep=' '):
	"""Return a string of the bytes in hex format"""
	return sep.join([ "%02X" % ord( x ) for x in byteString ])

def listToHex(byteList, sep=' '):
	"""Return a string of each number in hex format"""
	return sep.join("%02X" % x for x in byteList)