#!/usr/bin/python

import rc4
import utilities

def fms():
	"""Crack a WEP key using the FMS attack. Not entirely feasible, needs weak
	IVs, and most vendors filter them out."""

	##The actual key. Used for example purposes, to generate the keystream
	key = [1, 2, 3, 4, 5, 66, 75, 123, 99, 100, 123, 43, 213]

	#The part of the root key we know. Will be updated each round
	known = []

	#Repeat for each keybyte
	for A in range(13):
		results = [0 for x in range(256)]

		#Generate weak IVs
		iv = [A + 3, 255, 0]

		for x in xrange(256):
			iv[2] = x
			keystream = rc4.wepRC4(iv, key)

			S = range(256)
			K = iv[:]
			K.extend(known)

			#Perform all possible steps of the Key Scheduling Algorithm
			j = 0
			for i in xrange(A + 3):
				j = (j + S[i] + K[i]) % 256
				S[i], S[j] = S[j], S[i]
			
			if j < 2:
				#S[0] or S[1] has been disturbed, discard
				continue

			#This will be right 5% of the time. May not seem like a lot, but
			#better than random
			keybyte = keystream - j - S[A + 3]

			#Make the keybyte positive
			while keybyte < 0:
				keybyte += 256

			#Count the result as a 'vote' for that value
			results[keybyte] += 1

		#The most common vote is probably the keybyte
		count = max(results)
		most = results.index(count)

		print "Keybyte:", most
		known.append(most)

if __name__ == '__main__':
	fms()