import rc4
import utilities.permutations as perms

import itertools

class PTW(object):
	"""A convience class, encapsulates the PTW attack, as first described in
	'Breaking 104 bit WEP in 60 seconds' by Tews, Weinmann, and Pyshkin"""

	def __init__(self):
		super(PTW, self).__init__()
		self.results = [[0]*256 for i in xrange(13)]

	def update(self, iv, keystream):
		"""Update the vote arrays with a new IV and keystream"""

		#Set up the initial S box
		S = range(256)

		#Perform the first 3 steps of the RC4 key scheduling algorithm
		j = 0
		for i in xrange(3):
			j = (j + S[i] + iv[i]) % 256
			S[i], S[j] = S[j], S[i]

		#Invert the S box
		Sinverse = perms.inversePerm(S)

		#Find the guess for each sigma.
		#Tews, Weinmann, and Pyshkin (Equation 5)
		for i in xrange(13):
			#Sum to i+4 because the end index for a slice is not inclusive
			sigmaGuess = (Sinverse[i + 3 - keystream[i + 2]] - (j + sum(S[3:i+4]))) % 256
			self.results[i][sigmaGuess] += 1

	def crack(self, iv, keystream, limit=10000):
		"""Use all of the votes gathered from calling the update method to
		calculate the key. Will perform keyranking if the first guess isn't
		right, up to <limit> keys"""

		#Keys that have already been tried
		triedKeys = []
		
		#The number of votes for the top sigmas
		topValues = []

		#We change the arravy, so make a copy to preserve the old one.
		votes = [arr[:] for arr in self.results]

		#Find the most common values for each sigma
		possibleSigma = []
		for i in xrange(13):
			count = max(votes[i])
			possibleSigma.append([votes[i].index(count)])

			#Save the number of votes
			topValues.append(count)

			#Zero out the value, so it is no longer the largest
			votes[i][votes[i].index(count)] = 0

		#Try the key generated from the top sigmas first. If that doesn't work, move
		#on to key ranking
		answer = self.tryKey([possibleSigma[i][0] for i in xrange(13)], iv, keystream)
		if answer != []:
			return answer

		numKeysTried = 0

		#If the key was incorrect, add it to the list of tried keys
		triedKeys.append([possibleSigma[i][0] for i in xrange(13)])

		while numKeysTried < limit:
			#Add the values with the closest distance from the top point to the 
			#possible sigmas

			#The next highest values for each of the sigams
			possibleAdditions= [max(votes[i]) for i in xrange(13)]

			#The distance of those from the highest sigma
			distances = [topValues[i] - possibleAdditions[i] for i in xrange(13)]
			
			#Add the sigma with the smallest distance from top
			toAdd = distances.index(min(distances))
			newPossibility = votes[toAdd].index(possibleAdditions[toAdd])
			possibleSigma[toAdd].append(newPossibility)

			#Zero out the value to make room for the next one
			votes[toAdd][newPossibility] = 0

			#Iterate over all of the possible keys
			for possibility in itertools.product(*possibleSigma):
				
				if possibility in triedKeys:
					continue

				answer = self.tryKey(possibility, iv, keystream)
				if answer != []:
					return answer

				numKeysTried += 1
				if numKeysTried > limit:
					break
				triedKeys.append(possibility)
		return []

	def tryKey(self, sigmas, iv, keystream):
		"""Calculate the key from the sigmas (PTW) and test it against the keystream

		The keystream is expected to be 15 bytes long, and both the 40 bit key 
		and the 104 bit key are tested. If the key is correct, it is returned.
		Otherwise, an empty array is returned.
		"""
		
		#Calculate the key from the given sigmas
		guess = [0] * 13
		guess[0] = sigmas[0]
		for i in xrange(1, 13):
			guess[i] = (sigmas[i] - sigmas[i - 1]) % 256

		if rc4.wepRC4(iv, guess, 15) == keystream: #Check the 104bit key
			return guess
		elif rc4.wepRC4(iv, guess[:5], 15) == keystream: #Check the 40bit key
			return guess[:5]
		else:
			return []