#!/usr/bin/python

import rc4
import utilities

def klein():
	"""Crack a WEP key using Klein's correlation, as described in 
	'Klein's and PTW attacks on WEP'
	"""

	#The actual key. Used for example purposes, to generate the keystream
	key = [1, 2, 3, 7, 5, 66, 75, 123, 99, 100, 123, 43, 213]

	#The known keybytes, updated each round.
	known = []

	#Repeat for every keybyte
	for A in xrange(13):

		#The vote array for this byte.
		results = [0 for x in xrange(256)]

		#Status variable
		attempt = 0

		#Repeat a bunch, to gather enough information.
		for x in xrange(50000):
			#Just increment the IV each time
			iv = [(x / 256**2) % 256, (x / 256) % 256, x % 256]
			
			#Get the 15 bytes of the keystream, would normally be done by XORing
			#with known plaintext
			keystream = rc4.wepRC4(iv, key, 15)

			#Set up the S box and Key with known values.
			S = range(256)
			K = iv[:]
			K.extend(known)

			#Run the RC4 key scheduling algorithm as many times as possible
			j = 0
			for i in xrange(A + 3):
				j = (j + S[i] + K[i]) % 256
				S[i], S[j] = S[j], S[i]

			#Invert the S box
			Sinverse = utilities.inversePerm(S)

			#Calculate the possible keybyte (Stulbonov, equation 22)
			i = A + 3 #Change of variable to avoid confusion
			keybyte = (Sinverse[i - keystream[i - 1]] - (S[i] + j)) % 256

			#Count the result as a vote for that value
			results[keybyte] += 1
			
			#Status update
			attempt += 1
			if attempt % 10000 == 0:
				print "IVs:", attempt

		#Find the most common value
		count = max(results)
		most = results.index(count)
		
		#Assume that it is the correct keybyte
		print "Keybyte:", most
		known.append(most)